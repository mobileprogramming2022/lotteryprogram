import 'dart:io';

void main(List<String> arguments) {
  print("input lottery :");
  int lottery = int.parse(stdin.readLineSync()!);
  check(lottery);
}

void check(int lottery) {
  int X1 = 169457;
  int n31 = 103;
  int n32 = 666;
  int t31 = 017;
  int t32 = 238;
  int t2 = 26;
  int count = 0;
  if (lottery == X1) {
    print("1st prize 6,000,000 ฿");
    count++;
  }
  if ((lottery / 1000).toInt() == n31 || (lottery / 1000).toInt() == n32) {
    print("won 3 page numbers 4,000 ฿");
    count++;
  }
  if ((lottery - ((lottery / 1000).toInt() * 1000)) == t31 ||
      (lottery - ((lottery / 1000).toInt() * 1000)) == t32) {
    print("won the last 3 numbers 4,000 ฿");
    count++;
  }
  if ((lottery - ((lottery / 100).toInt() * 100)) == t2) {
    print("won the last 2 numbers89 2,000 ฿");
    count++;
  }
  if (count == 0) {
    print("You are not rewarded the lottery.");
  }
}
